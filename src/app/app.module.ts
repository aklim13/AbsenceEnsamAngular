import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {LoginComponent} from './Components/login/login.component';
import {RouterModule, Routes} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {HttpModule} from '@angular/http';
import {AuthService} from './Services/auth.service';
import {AuthGuard} from './Guards/auth.guard';
import {NavbarService} from './Services/navbar.service';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {DataService} from './Services/data.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {EtudiantComponent} from './Components/etudiant/etudiant.component';
import {SeanceComponent} from './Components/seance/seance.component';
import {ProfesseurComponent} from './Components/professeur/professeur.component';
import {AbsenceComponent} from './Components/absence-detail/absence-detail.component';
import {NavbarComponent} from './Components/navbar-admin/navbar-admin.component';
import {AbsenceFormComponent} from './Components/absence-form/absence-form.component';
import {SearchComponent} from './Components/search/search.component';
import {ParentModalComponent} from './Components/parent-modal/parent-modal.component';
import {CoursComponent} from './Components/cours/cours.component';
import {SeanceAbsenceComponent} from './Components/seance-absence/seance-absence.component';
import {NavbarEtudiantComponent} from './Components/navbar-etudiant/navbar-etudiant.component';
import {NavbarProfesseurComponent} from './Components/navbar-professeur/navbar-professeur.component';
import {TofirstpageGuard} from './Guards/tofirstpage.guard';
import {NotAllowedComponent} from './Components/Error/not-allowed/not-allowed.component';
import {ProfileProfComponent} from './Components/profile-prof/profile-prof.component';
import {ProfileEtudiantComponent} from './Components/profile-etudiant/profile-etudiant.component';
import {SettingsComponent} from './Components/settings/settings.component';
import {PasswordEditComponent} from './Components/password-edit/password-edit.component';
import {ProfileAdminComponent} from './Components/profile-admin/profile-admin.component';
import {InlineEditorModule} from '@qontu/ngx-inline-editor';
import {AbsenceEtudiantComponent} from './Components/absence-etudiant/absence-etudiant.component';
import {AbsenceListComponent} from './Components/absence-list/absence-list.component';
import {ShareDataService} from './Services/share-data.service';
import {NavbarDetailsComponent} from './Components/navbar-details/navbar-details.component';
import {EtudiantStatsComponent} from './Components/etudiant-stats/etudiant-stats.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FilterStatsEtudiantComponent} from './Components/filter-stats-etudiant/filter-stats-etudiant.component';
import {NavStatsComponent} from './Components/nav-stats/nav-stats.component';
import {SingleStatsEtudiantComponent} from './Components/single-stats-etudiant/single-stats-etudiant.component';
import {AbsenceModalComponent} from './Components/absence-modal/absence-modal.component';
import {MatButtonModule, MatCheckboxModule, MatListModule} from '@angular/material';
import {TestingComponent} from './Components/testing/testing.component';
import {ExcelService} from './Services/excel.service';


const appRoutes: Routes = [
  {path: '', component: AppComponent, canActivate: [TofirstpageGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'testing', component: TestingComponent},
  {path: 'etudiants', component: EtudiantComponent, canActivate: [AuthGuard]},
  {path: 'etudiant/statistiques', component: SingleStatsEtudiantComponent, canActivate: [AuthGuard]},
  {path: 'seances', component: SeanceComponent, canActivate: [AuthGuard]},
  {path: 'absences', component: AbsenceEtudiantComponent, canActivate: [AuthGuard]},
  {path: 'professeurs', component: ProfesseurComponent, canActivate: [AuthGuard]},
  {path: 'liste/cours', component: AbsenceFormComponent, canActivate: [AuthGuard]},
  {path: 'noter/absence', component: AbsenceListComponent, canActivate: [AuthGuard]},
  {path: 'etudiants/:id/absences', component: AbsenceComponent, canActivate: [AuthGuard]},
  {path: 'seances/:id/absences', component: SeanceAbsenceComponent, canActivate: [AuthGuard]},
  {path: 'professeurs/:id/modules', component: CoursComponent, canActivate: [AuthGuard]},
  {path: 'etudiants/:id/stats', component: EtudiantStatsComponent, canActivate: [AuthGuard]},
  {
    path: 'parametres', component: SettingsComponent, canActivate: [AuthGuard],
    children: [
      {path: 'password', component: PasswordEditComponent, canActivate: [AuthGuard]},
      {path: 'profile/professeur', component: ProfileProfComponent, canActivate: [AuthGuard]},
      {path: 'profile/etudiant', component: ProfileEtudiantComponent, canActivate: [AuthGuard]},
      {path: 'profile/admin', component: ProfileAdminComponent, canActivate: [AuthGuard]}
    ]
  },
  {path: 'notallowed', component: NotAllowedComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    ProfesseurComponent,
    EtudiantComponent,
    SeanceComponent,
    SearchComponent,
    AbsenceFormComponent,
    ParentModalComponent,
    AbsenceComponent,
    CoursComponent,
    SeanceAbsenceComponent,
    NavbarEtudiantComponent,
    NavbarProfesseurComponent,
    NotAllowedComponent,
    ProfileProfComponent,
    ProfileEtudiantComponent,
    SettingsComponent,
    PasswordEditComponent,
    ProfileAdminComponent,
    AbsenceEtudiantComponent,
    AbsenceListComponent,
    NavbarDetailsComponent,
    EtudiantStatsComponent,
    FilterStatsEtudiantComponent,
    NavStatsComponent,
    SingleStatsEtudiantComponent,
    AbsenceModalComponent,
    TestingComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpModule,
    MatListModule,
    MatButtonModule,
    NgxDatatableModule,
    InlineEditorModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatButtonModule,
    NgbModule.forRoot()
  ],
  providers: [
    CookieService,
    AuthService,
    TofirstpageGuard,
    AuthGuard,
    NavbarService,
    DataService,
    ExcelService,
    ShareDataService
  ],
  bootstrap: [AppComponent],
  entryComponents: [ParentModalComponent, AbsenceModalComponent]
})
export class AppModule {
}
