import {Component, OnInit} from '@angular/core';
import {NavbarService} from '../../Services/navbar.service';
import {DataService} from '../../Services/data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-single-stats-etudiant',
  templateUrl: './single-stats-etudiant.component.html',
  styleUrls: ['./single-stats-etudiant.component.css']
})
export class SingleStatsEtudiantComponent implements OnInit {
  showStats = {'hori': false, 'verti': true, 'donut': false, 'donut2': false, 'graphe': false};
  stats = [];
  colorScheme = {
    domain: [
      '#3366CC',
      '#DC3912',
      '#FF9900',
      '#109618',
      '#990099',
      '#3B3EAC',
      '#0099C6',
      '#DD4477',
      '#66AA00',
      '#B82E2E',
      '#316395',
      '#994499',
      '#22AA99',
      '#AAAA11',
      '#6633CC',
      '#E67300',
      '#8B0707',
      '#329262',
      '#5574A6',
      '#3B3EAC',
    ]
  };
  title: string;
  etudiantID: number;

  constructor(public nav: NavbarService,
              private dataService: DataService,
              private router: Router) {
  }

  shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
  }

  ngOnInit() {
    this.nav.show();
    this.shuffle(this.colorScheme.domain);
    this.dataService.getCurrentProfile().subscribe(
      etudiant => {
        this.title = etudiant.AspNetUsers.Prenom + ' ' + etudiant.AspNetUsers.Nom;
        this.etudiantID = etudiant.EtudiantID;
      },
      err => {
        if (err.status === 401) {
          this.router.navigate(['/notallowed']);
        }
      }
    );
  }

  onStatsObtained(data) {
    this.stats = data.mystats;
  }

}
