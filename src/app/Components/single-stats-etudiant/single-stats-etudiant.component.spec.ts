import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SingleStatsEtudiantComponent} from './single-stats-etudiant.component';

describe('SingleStatsEtudiantComponent', () => {
  let component: SingleStatsEtudiantComponent;
  let fixture: ComponentFixture<SingleStatsEtudiantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SingleStatsEtudiantComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleStatsEtudiantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
