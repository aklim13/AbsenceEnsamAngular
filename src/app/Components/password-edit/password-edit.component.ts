import {Component, OnInit} from '@angular/core';
import {DataService} from '../../Services/data.service';
import {NavbarService} from '../../Services/navbar.service';

@Component({
  selector: 'app-password-edit',
  templateUrl: './password-edit.component.html',
  styleUrls: ['./password-edit.component.css']
})
export class PasswordEditComponent implements OnInit {
  changePassword = {OldPassword: '', NewPassword: '', ConfirmPassword: ''};
  successMessage: string;
  warningMessage: string;

  constructor(private dataService: DataService,
              private nav: NavbarService) {
  }

  ngOnInit() {
    this.nav.show();
  }

  resetFields() {
    this.changePassword.ConfirmPassword = this.changePassword.OldPassword = this.changePassword.NewPassword = '';
  }

  onSubmit() {
    this.dataService.changePassword(this.changePassword).subscribe(
      result => {
        this.successMessage = 'Votre mot de passe est changé';
        this.resetFields();
      },
      err => {
        this.warningMessage = 'Ancien mot de passe est incorrect';
        this.resetFields();
      },
      () => {
      }
    );
  }
}
