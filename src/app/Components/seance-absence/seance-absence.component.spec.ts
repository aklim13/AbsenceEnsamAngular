import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SeanceAbsenceComponent} from './seance-absence.component';

describe('SeanceAbsenceComponent', () => {
  let component: SeanceAbsenceComponent;
  let fixture: ComponentFixture<SeanceAbsenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SeanceAbsenceComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeanceAbsenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
