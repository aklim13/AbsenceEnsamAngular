import {Component, OnInit, ViewChild} from '@angular/core';
import {DataService} from '../../Services/data.service';
import {NavbarService} from '../../Services/navbar.service';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {ExcelService} from '../../Services/excel.service';

@Component({
  selector: 'app-seance-absence',
  templateUrl: './seance-absence.component.html',
  styleUrls: ['./seance-absence.component.css']
})
export class SeanceAbsenceComponent implements OnInit {
  @ViewChild('myTable') table: any;
  loadingIndicator = true;
  absences: Absence[];
  tmpAbsences: Absence[];
  seance: Seance;
  professeur: Professeur;
  title: string;

  constructor(private nav: NavbarService,
              private dataService: DataService,
              private location: Location,
              private excelService: ExcelService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.nav.hide();
    this.dataService.getSeance(this.route.snapshot.params['id']).subscribe(
      seance => {
        this.seance = seance;
        this.title = seance.DetailModule.Module.description + ' : ' + seance.DetailModule.Cours.Description;
      }
    );
    this.dataService.getCurrentProfile().flatMap(
      professeur => {
        this.professeur = professeur;
        return this.dataService.getSeanceAbsences(this.route.snapshot.params['id']);
      }
    ).subscribe(
      absences => {
        this.absences = absences;
        this.tmpAbsences = absences;
        console.log(this.absences);
      },
      err => {
        this.loadingIndicator = false;
      },
      () => {
        this.loadingIndicator = false;
      }
    );
  }

  onAbsenceFiltred(data) {
    this.absences = data.filteredList;
  }

  onReportClicked() {
    const absenceReport = [];
    this.absences.forEach(
      abs => {
        absenceReport.push({
          Nom: abs.Etudiant.AspNetUsers.Nom,
          Prenom: abs.Etudiant.AspNetUsers.Prenom,
          Email: abs.Etudiant.AspNetUsers.Email,
          Cin: abs.Etudiant.AspNetUsers.Cin,
          Tel: abs.Etudiant.AspNetUsers.PhoneNumber,
          Type: abs.Seance.SeanceType.description,
          'Nombre Seance': abs.Seance.NbrSeance
        });
      }
    );
    this.excelService.exportAsExcelFile(absenceReport,
      this.title + '_' + this.absences[0].NomProfesseur);
  }
}
