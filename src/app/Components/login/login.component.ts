import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../Services/auth.service';
import {NavbarService} from '../../Services/navbar.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginData = {email: '', password: ''};
  warningMessage: string;

  constructor(private authService: AuthService, private nav: NavbarService) {
  }

  ngOnInit() {
    this.nav.hide();
    if (this.authService.isLoggedIn()) {
      this.authService.sendToFirstPage();
    }
  }

  onSubmit() {
    this.authService.getTokens(this.loginData).subscribe(
      data => this.authService.saveToken(data),
      err => this.warningMessage = 'Votre email ou mot de passe est incorrect !',
      () => {

      }
    );
  }
}
