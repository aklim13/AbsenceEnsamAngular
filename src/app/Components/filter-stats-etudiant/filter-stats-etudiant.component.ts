import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {DataService} from '../../Services/data.service';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgbDate} from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';

const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day > two.day : one.month > two.month : one.year > two.year;

@Component({
  selector: 'app-filter-stats-etudiant',
  templateUrl: './filter-stats-etudiant.component.html',
  styleUrls: ['./filter-stats-etudiant.component.css'],
})
export class FilterStatsEtudiantComponent implements OnInit, OnChanges {
  @Input() etudiantID: number;
  @Output() outStats = new EventEmitter<{ mystats: any[] }>();
  showSettings = {
    isEnabled: false,
    hideDate: 'Cacher date',
    hideDateIcon: 'indeterminate_check_box',
    showDate: 'Changer date',
    showDateIcon: 'date_range'
  };
  hoveredDate: NgbDateStruct;
  fromDate: NgbDateStruct;
  toDate: NgbDateStruct;

  constructor(private dataService: DataService) {
  }

  ngOnInit() {

  }

  ngOnChanges() {
    this.showWeekStats();
  }

  isHovered = date => this.fromDate && !this.toDate && this.hoveredDate && after(date, this.fromDate) && before(date, this.hoveredDate);

  isInside = date => after(date, this.fromDate) && before(date, this.toDate);
  isFrom = date => equals(date, this.fromDate);
  isTo = date => equals(date, this.toDate);

  onDateChange(date: NgbDateStruct) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && after(date, this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
    this.showAnyDateStats();
  }

  private showWeekStats() {
    this.dataService.getEtudiantStatsByWeek(this.etudiantID)
      .subscribe(
        (stats: Stats) => {
          this.fromDate = new NgbDate(stats.BeginDate.Year, stats.BeginDate.Month, stats.BeginDate.Day);
          this.toDate = new NgbDate(stats.EndDate.Year, stats.EndDate.Month, stats.EndDate.Day);
          this.outStats.emit({mystats: stats.Stats});
        },
        err => {
          console.log(err);
        },
        () => {
        }
      );
  }

  private showAnyDateStats() {
    this.dataService.getEtudiantStatsByDate(
      this.etudiantID,
      `${this.fromDate.month}/${this.fromDate.day}/${this.fromDate.year}`,
      `${this.toDate.month}/${this.toDate.day}/${this.toDate.year}`
    )
      .subscribe(
        (stats: Stats) => {
          this.fromDate = new NgbDate(stats.BeginDate.Year, stats.BeginDate.Month, stats.BeginDate.Day);
          this.toDate = new NgbDate(stats.EndDate.Year, stats.EndDate.Month, stats.EndDate.Day);
          this.outStats.emit({mystats: stats.Stats});
        },
        err => {
          console.log(err);
        },
        () => {
        }
      );
  }
}
