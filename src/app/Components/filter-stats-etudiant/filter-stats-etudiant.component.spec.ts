import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FilterStatsEtudiantComponent} from './filter-stats-etudiant.component';

describe('FilterStatsEtudiantComponent', () => {
  let component: FilterStatsEtudiantComponent;
  let fixture: ComponentFixture<FilterStatsEtudiantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FilterStatsEtudiantComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterStatsEtudiantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
