import {Component, OnInit, ViewChild} from '@angular/core';
import {NavbarService} from '../../Services/navbar.service';
import {DataService} from '../../Services/data.service';
import {ActivatedRoute} from '@angular/router';
import 'rxjs/add/operator/mergeMap';
import {ExcelService} from '../../Services/excel.service';

@Component({
  selector: 'app-absence-detail',
  templateUrl: './absence-detail.component.html',
  styleUrls: ['./absence-detail.component.css']
})
export class AbsenceComponent implements OnInit {
  etudiant: Etudiant;
  @ViewChild('myTable') table: any;
  loadingIndicator = true;
  absences: Absence[];
  tmpAbsences: Absence[];
  columns = [];
  title: string;

  constructor(private nav: NavbarService,
              private dataService: DataService,
              private excelService: ExcelService,
              private route: ActivatedRoute) {
    this.columns = [
      {prop: 'Seance.NbrSeance', name: 'Nombre seance', width: 50},
      {prop: 'Seance.Date', name: 'Date'},
      {prop: 'Seance.DetailModule.Module.description', name: 'Module'},
      {prop: 'Seance.DetailModule.Cours.Description', name: 'Cours'}
    ];
  }

  ngOnInit() {
    this.nav.hide();
    this.dataService.getEtudiantsById(this.route.snapshot.params['id']).flatMap(
      etudiant => {
        this.etudiant = etudiant;
        this.title = etudiant.AspNetUsers.Prenom + ' ' + etudiant.AspNetUsers.Nom;
        return this.dataService.getEtudiantAbsences(etudiant.EtudiantID);
      }
    ).subscribe(
      absences => {
        this.absences = absences;
        this.tmpAbsences = absences;
      },
      err => {
        console.log(err);
        this.loadingIndicator = false;
      },
      () => {
        this.loadingIndicator = false;
      }
    );
  }

  onAbsenceFiltred(data) {
    this.absences = data.filteredList;
  }

  onReportClicked() {
    const etudiantAbsenceReport = [];
    this.dataService.getEtudiantAbsencesReport(this.route.snapshot.params['id']).subscribe(
      reports => {
        reports.forEach(
          (report: Absence) => {
            etudiantAbsenceReport.push(
              {
                Nom: report.Seance.DetailModule.Professeur.AspNetUsers.Nom,
                Prenom: report.Seance.DetailModule.Professeur.AspNetUsers.Prenom,
                Annee: report.Seance.DetailModule.DetailCycle.Annee.description,
                Filliere: report.Seance.DetailModule.DetailCycle.OptionFilliere.Filliere.Description,
                Option: report.Seance.DetailModule.DetailCycle.OptionFilliere.Option,
                Cycle: report.Seance.DetailModule.DetailCycle.Cycle.Description,
                Type: report.Seance.SeanceType.description,
                date: report.Seance.Date,
                Cin: report.Seance.DetailModule.Professeur.AspNetUsers.Cin,
                Tel: report.Seance.DetailModule.Professeur.AspNetUsers.PhoneNumber,
                Email: report.Seance.DetailModule.Professeur.AspNetUsers.Email,
                Departement: report.Seance.DetailModule.Professeur.Departement.Description,
                Groupe: report.Seance.DetailModule.DetailCycle.Groupe.description,
                Section: report.Seance.DetailModule.DetailCycle.Section.description,
                Vacataire: report.Seance.DetailModule.Professeur.Vacataire ? 'vacataire' : 'permanant'
              }
            );
          }
        );
      },
      err => {
        console.log(err);
      },
      () => {
        console.log(etudiantAbsenceReport);
        this.excelService.exportAsExcelFile(etudiantAbsenceReport,
          this.etudiant.AspNetUsers.Nom + '_' + this.etudiant.AspNetUsers.Prenom);
      }
    );
  }
}
