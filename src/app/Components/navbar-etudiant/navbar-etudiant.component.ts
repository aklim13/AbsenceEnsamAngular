import {Component, OnInit} from '@angular/core';
import {NavbarService} from '../../Services/navbar.service';
import {AuthService} from '../../Services/auth.service';
import {DataService} from '../../Services/data.service';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-navbar-etudiant',
  templateUrl: './navbar-etudiant.component.html',
  styleUrls: ['./navbar-etudiant.component.css']
})
export class NavbarEtudiantComponent implements OnInit {
  etudiant: Etudiant;

  constructor(private auth: AuthService,
              public nav: NavbarService,
              private dataService: DataService,
              private router: Router) {
  }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.dataService.getCurrentProfile().subscribe(
          etudiant => {
            this.etudiant = etudiant;
          }
        );
      }
    });
  }

  onNavbarClicked() {
    this.nav.toggleNavbar();
  }

  onLogoutClicked() {
    this.auth.logout();
    this.nav.hideAll();
  }
}
