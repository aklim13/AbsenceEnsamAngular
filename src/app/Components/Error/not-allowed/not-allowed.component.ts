import {Component, OnInit} from '@angular/core';
import {NavbarService} from '../../../Services/navbar.service';

@Component({
  selector: 'app-not-allowed',
  templateUrl: './not-allowed.component.html',
  styleUrls: ['./not-allowed.component.css']
})
export class NotAllowedComponent implements OnInit {

  constructor(private nav: NavbarService) {
  }

  ngOnInit() {
    this.nav.hide();
  }

}
