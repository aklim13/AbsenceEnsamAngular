import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {DataService} from '../../Services/data.service';
import {ShareDataService} from '../../Services/share-data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-absence-modal',
  templateUrl: './absence-modal.component.html',
  styleUrls: ['./absence-modal.component.css']
})
export class AbsenceModalComponent implements OnInit {
  @Input() detailModule: any;
  @Input() profName: string;
  selectedNombreSeance: number;
  nombreSeances = [
    {nbrSeance: 1, text: '1 seance'},
    {nbrSeance: 2, text: '2 seances'},
    {nbrSeance: 3, text: '3 seances'},
    {nbrSeance: 4, text: '4 seances'},
  ];

  constructor(public activeModal: NgbActiveModal,
              private dataService: DataService,
              private router: Router,
              private shareData: ShareDataService) {
  }

  ngOnInit() {
    this.selectedNombreSeance = 1;
  }

  onCloseClicked() {
    this.activeModal.close('Close click');
  }

  onSaveClicked() {
    const seance = {
      DetailModuleID: this.detailModule.DetailModuleID,
      NbrSeance: this.selectedNombreSeance
    };
    this.dataService.postSeance(seance).subscribe(
      res => {
        this.detailModule.seanceID = res.SeanceID;
        this.shareData.changeSeanceMessage(this.detailModule, this.profName);
        this.router.navigate(['/noter/absence']);
      },
      err => {
        console.log(err);
      },
      () => {
        this.activeModal.close('Close click');
      }
    );
  }
}
