import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {NavbarService} from '../../Services/navbar.service';
import {DataService} from '../../Services/data.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-cours',
  templateUrl: './cours.component.html',
  styleUrls: ['./cours.component.css']
})
export class CoursComponent implements OnInit {
  professeur: Professeur;
  modules: DetailModule[];
  @ViewChild('myTable') table: any;
  loadingIndicator = true;
  tmpDetailModuel: DetailModule[];
  columns = [];
  title: string;

  constructor(private router: ActivatedRoute,
              private nav: NavbarService,
              private location: Location,
              private route: ActivatedRoute,
              private dataService: DataService) {
    this.columns = [
      {prop: 'DetailCycle.Cycle.Description', name: 'Cycle'},
      {prop: 'DetailCycle.Annee.description', name: 'Cycle'},
      {prop: 'Module.description', name: 'Module'},
      {prop: 'Cours.Description', name: 'Cours'},
      {prop: 'DetailCycle.OptionFilliere.Filliere.Description', name: 'Filliere'},
      {prop: 'DetailCycle.OptionFilliere.Option', name: 'OptionFilliere'},
      {prop: 'DetailCycle.Section.description', name: 'Section'}
    ];
  }

  ngOnInit() {
    this.nav.hide();
    this.dataService.getProfesseur(this.route.snapshot.params['id']).subscribe(
      professeur => {
        this.professeur = professeur;
        this.title = this.professeur.AspNetUsers.Prenom + ' ' + this.professeur.AspNetUsers.Nom;
      }
    );
    this.dataService.getProfesseurModules(this.route.snapshot.params['id']).subscribe(
      modules => {
        this.modules = modules;
        console.log(modules);
        this.tmpDetailModuel = modules;
      },
      err => {
        console.log(err);
        this.loadingIndicator = false;
      },
      () => {
        this.loadingIndicator = false;
      }
    );
  }

  onAbsenceFiltred(data) {
    this.modules = data.filteredList;
  }
}
