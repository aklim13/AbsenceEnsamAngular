import {Component, OnInit} from '@angular/core';
import {DataService} from '../../Services/data.service';
import {NavbarService} from '../../Services/navbar.service';

@Component({
  selector: 'app-profile-etudiant',
  templateUrl: './profile-etudiant.component.html',
  styleUrls: ['./profile-etudiant.component.css']
})
export class ProfileEtudiantComponent implements OnInit {
  etudiant: Etudiant;
  columns = [];
  detailCycle: DetailCycle;

  constructor(private nav: NavbarService,
              public dataService: DataService) {
  }

  ngOnInit() {
    this.nav.show();
    this.dataService.getCurrentProfile().flatMap(
      etudiant => {
        this.etudiant = etudiant;
        return this.dataService.getEtudiantDetailCycle(etudiant);
      }
    ).subscribe(detailCycle => {
      this.detailCycle = detailCycle;
    }, err => {
    }, () => {

    });
  }
}
