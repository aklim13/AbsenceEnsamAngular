import {Component, OnInit} from '@angular/core';
import {DataService} from '../../Services/data.service';
import {NavbarService} from '../../Services/navbar.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-profile-admin',
  templateUrl: './profile-admin.component.html',
  styleUrls: ['./profile-admin.component.css']
})
export class ProfileAdminComponent implements OnInit {
  admin: AspNetUsers;

  constructor(private nav: NavbarService,
              public dataService: DataService,
              private router: Router) {
  }

  ngOnInit() {
    this.nav.show();
    this.dataService.getCurrentProfile().subscribe(
      admin => {
        this.admin = admin;
      },
      err => {
        console.log(err);
      }
    );
  }

}
