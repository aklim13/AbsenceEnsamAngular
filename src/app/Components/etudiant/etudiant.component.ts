import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {NavbarService} from '../../Services/navbar.service';
import {DataService} from '../../Services/data.service';
import {ParentModalComponent} from '../parent-modal/parent-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {ExcelService} from '../../Services/excel.service';

@Component({
  selector: 'app-etudiant',
  templateUrl: './etudiant.component.html',
  styleUrls: ['./etudiant.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class EtudiantComponent implements OnInit {
  @ViewChild('myTable') table: any;
  etudiantDetail: DetailEtudiant;
  loadingIndicator = true;
  isToggled = false;
  lastRowClicked = 0;
  etudiants: Etudiant[];
  tempEtudiant: Etudiant[];
  currentRow: Etudiant;
  height: number;
  width: number;

  constructor(private nav: NavbarService,
              public dataService: DataService,
              public modalService: NgbModal,
              private exelService: ExcelService,
              private router: Router) {
    this.etudiantDetail = {Cycle: '', Filliere: '', OptionFilliere: '', Section: '', Annee: '', Groupe: ''};
  }

  ngOnInit() {
    this.nav.show();
    this.dataService.getEtudiants().subscribe(
      etudiants => {
        this.tempEtudiant = [...etudiants];
        this.etudiants = etudiants;
        console.log(etudiants);
      },
      err => {
        console.log(err);
        if (err.status === 401) {
          this.router.navigate(['/notallowed']);
        }
      },
      () => {
        this.loadingIndicator = false;
      });
  }

  onDetailToggle(event) {
    if (!this.isToggled) {
      this.dataService.getEtudiantsData(event.value).subscribe(data => {
        this.etudiantDetail.Cycle = data[0];
        this.etudiantDetail.Annee = data[1];
        this.etudiantDetail.Filliere = data[2].Filliere.Description;
        this.etudiantDetail.OptionFilliere = data[2].Option;
        this.etudiantDetail.Groupe = data[3];
        this.etudiantDetail.Section = !data[4] ? '-' : data[4];
      });
    }
    this.currentRow = event.value;
    this.isToggled = !this.isToggled;
  }

  onActivate(event) {
    if (event.type === 'click') {
      if (event.value === this.lastRowClicked) {
        this.table.rowDetail.toggleExpandRow(event.row);
      } else {
        this.table.rowDetail.collapseAllRows();
        this.table.rowDetail.toggleExpandRow(event.row);
        this.lastRowClicked = event.value;
      }
    }
  }

  open() {
    const modalRef = this.modalService.open(ParentModalComponent, {
      size: 'lg'
    });
    modalRef.componentInstance.nomEtudiant = this.currentRow.AspNetUsers.Prenom + ' ' + this.currentRow.AspNetUsers.Nom;
    this.dataService.getEtudiantParents(this.currentRow.EtudiantID).subscribe(
      parents => {
        modalRef.componentInstance.parents = parents;
      },
      err => {
        console.log(err);
      },
      () => {

      }
    );

  }

  onEtudiantFiltred(data) {
    this.etudiants = data.filteredList;
  }

  onSizeChanged() {
    if (window.innerWidth < 992) {
      this.height = 400;
    }
    if (window.innerWidth < 768) {
      this.height = 500;
    }
  }

  showEtudiantStatistics() {
    this.router.navigate([`/etudiants/${this.currentRow.EtudiantID}/stats`]);
  }

  showEtudiantAbsences() {
    this.router.navigate([`/etudiants/${this.currentRow.EtudiantID}/absences`]);
  }

  onButtonClicked() {
    const downloadEtudiants = [];
    this.dataService.getReportEtudiants().subscribe(
      etudreport => {
        etudreport.forEach(
          r => {
            downloadEtudiants.push({
              Nom: r.AspNetUsers.Nom,
              Prenom: r.AspNetUsers.Prenom,
              Adresse: r.Adresse,
              Absences: r.AbsenceCount,
              Année: r.DetailCycle.Annee.description,
              Cycle: r.DetailCycle.Cycle.Description,
              Groupe: r.DetailCycle.Groupe.description,
              Filliere: r.DetailCycle.OptionFilliere.Filliere.Description,
              OptionFilliere: r.DetailCycle.OptionFilliere.Option,
              Section: r.DetailCycle.Section.description
            });
          }
        );

      },
      err => {

      },
      () => {
        console.log(downloadEtudiants);
        this.exelService.exportAsExcelFile(downloadEtudiants, 'etudiants');
      }
    );
  }
}
