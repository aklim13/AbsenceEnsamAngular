import {Component, OnInit, ViewChild} from '@angular/core';
import {NavbarService} from '../../Services/navbar.service';
import {DataService} from '../../Services/data.service';
import {Router} from '@angular/router';
import {ExcelService} from '../../Services/excel.service';

@Component({
  selector: 'app-seance',
  templateUrl: './seance.component.html',
  styleUrls: ['./seance.component.css']
})
export class SeanceComponent implements OnInit {
  @ViewChild('myTable') table: any;
  loadingIndicator = true;
  isToggled = false;
  seances: Seance[];
  tmpSeances: Seance[];

  constructor(private nav: NavbarService,
              private dataService: DataService,
              private excelService: ExcelService,
              private router: Router) {
  }

  ngOnInit() {
    this.nav.show();
    this.dataService.getSeances().subscribe(
      seances => {
        this.seances = seances;
        this.tmpSeances = [...seances];
      },
      err => {
        console.log(err);
      },
      () => {
        this.loadingIndicator = false;
      }
    );
  }

  onActivate(event) {
    if (event.type === 'click') {
      this.router.navigate([`/seances/${event.row.SeanceID}/absences`]);
    }
  }

  onEtudiantFiltred(data) {
    this.seances = data.filteredList;
  }

  onReportClicked() {
    const seanceReports = [];
    this.dataService.getSeanceReport().subscribe(
      reports => {
        reports.forEach(
          (seance: Seance) => {
            seanceReports.push({
              'Nombre Seance': seance.NbrSeance,
              Nom: seance.DetailModule.Professeur.AspNetUsers.Nom,
              Prenom: seance.DetailModule.Professeur.AspNetUsers.Prenom,
              Email: seance.DetailModule.Professeur.AspNetUsers.Email,
              Type: seance.SeanceType.description,
              Annee: seance.DetailModule.DetailCycle.Annee.description,
              Cycle: seance.DetailModule.DetailCycle.Cycle.Description,
              Filliere: seance.DetailModule.DetailCycle.OptionFilliere.Filliere.Description,
              date: seance.Date,
              Cin: seance.DetailModule.Professeur.AspNetUsers.Cin,
              Tel: seance.DetailModule.Professeur.AspNetUsers.PhoneNumber,
              Departement: seance.DetailModule.Professeur.Departement.Description,
              Groupe: seance.DetailModule.DetailCycle.Groupe.description,
              Section: seance.DetailModule.DetailCycle.Section.description,
              Option: seance.DetailModule.DetailCycle.OptionFilliere.Option,
              Vacataire: seance.DetailModule.Professeur.Vacataire ? 'vacataire' : 'permanant'
            });
          });
      }
      , err => {

      }, () => {
        this.excelService.exportAsExcelFile(seanceReports, 'professeurs');
      }
    );
  }

}
