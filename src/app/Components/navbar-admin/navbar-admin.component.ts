import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../Services/auth.service';
import {NavbarService} from '../../Services/navbar.service';
import {DataService} from '../../Services/data.service';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar-admin.component.html',
  styleUrls: ['./navbar-admin.component.css']
})
export class NavbarComponent implements OnInit {
  admin: AspNetUsers;

  constructor(private auth: AuthService,
              public nav: NavbarService,
              private dataService: DataService,
              private router: Router) {
  }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.dataService.getCurrentProfile().subscribe(
          admin => {
            this.admin = admin;
          }
        );
      }
    });

  }

  onNavbarClicked() {
    this.nav.toggleNavbar();
  }

  onLogoutClicked() {
    this.auth.logout();
    this.nav.hideAll();
  }
}
