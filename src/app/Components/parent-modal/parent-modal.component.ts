import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-etudiant-filter',
  templateUrl: './parent-modal.component.html',
  styleUrls: ['./parent-modal.component.css']
})
export class ParentModalComponent implements OnInit {
  @Input() parents: Parent[];
  @Input() nomEtudiant: string;

  constructor(public activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

}
