import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.css']
})
export class TestingComponent implements OnInit {
  etudiants = [
    {isChecked: false},
    {isChecked: false},
    {isChecked: false},
    {isChecked: false},
  ];

  constructor() {
  }

  ngOnInit() {
  }

  testinghh(event) {
    console.log(this.etudiants);
  }
}
