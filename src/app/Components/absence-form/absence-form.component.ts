import {Component, OnInit, ViewChild} from '@angular/core';
import {DataService} from '../../Services/data.service';
import {NavbarService} from '../../Services/navbar.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AbsenceModalComponent} from '../absence-modal/absence-modal.component';

@Component({
  selector: 'app-absence-form',
  templateUrl: './absence-form.component.html',
  styleUrls: ['./absence-form.component.css']
})
export class AbsenceFormComponent implements OnInit {
  professeur: Professeur;
  @ViewChild('myTable') table: any;
  detailModule: DetailModule[];
  tmpDetailModule: DetailModule[];
  loadingIndicator = true;
  columns = [];
  data: any;

  constructor(public dataService: DataService,
              private modalService: NgbModal,
              private nav: NavbarService) {
    this.columns = [
      {prop: 'DetailCycle.Groupe.description', name: 'Groupe', width: 80},
      {prop: 'DetailCycle.Cycle.Description', name: 'Cycle'},
      {prop: 'DetailCycle.Annee.description', name: 'Année'},
      {prop: 'DetailCycle.OptionFilliere.Filliere.Description', name: 'Filliere'},
      {prop: 'Module.description', name: 'Module'},
      {prop: 'Cours.Description', name: 'Cours'},
      {prop: 'DetailCycle.Section.description', name: 'Section', width: 80},
      {prop: 'DetailCycle.OptionFilliere.Option', name: 'Option', width: 80}
    ];
  }

  ngOnInit() {
    this.nav.show();
    this.dataService.getCurrentProfile().flatMap(professeur => {
      this.professeur = professeur;
      return this.dataService.getProfesseurModules(this.professeur.ProfesseurID);
    }).subscribe(detailModule => {
        this.detailModule = detailModule;
        this.tmpDetailModule = detailModule;
        console.log(detailModule);
      },
      err => {
        this.loadingIndicator = false;
      },
      () => {
        this.loadingIndicator = false;
      });
  }

  onCoursFiltred(data) {
    this.detailModule = data.filteredList;
  }

  onActivate(event) {
    if (event.type === 'click') {
      const modalRef = this.modalService.open(AbsenceModalComponent, {
        size: 'lg'
      });
      modalRef.componentInstance.detailModule = event.row;
      modalRef.componentInstance.profName = this.professeur.AspNetUsers.Prenom + ' ' +
        this.professeur.AspNetUsers.Nom;
    }
  }

}
