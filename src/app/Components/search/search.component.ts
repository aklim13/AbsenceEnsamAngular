import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  @Input() lists: any[];
  @Input() templist: any[];
  @Input() table: any;
  @Input() seance: Seance;
  @Input() hideReport: boolean;
  @Output() reportClicked = new EventEmitter<any>();
  @Output() filterList = new EventEmitter<{ filteredList: any[] }>();

  constructor() {
    this.hideReport = false;
  }

  ngOnInit() {
  }

  searchTable(event) {
    const val = event.target.value.toLowerCase();
    this.lists = this.templist.filter(etudiant =>
      JSON.stringify(etudiant)
        .replace(new RegExp('{', 'g'), ' ')
        .replace(new RegExp('}', 'g'), ' ')
        .replace(new RegExp('"', 'g'), '')
        .toLowerCase()
        .indexOf(val) !== -1 || !val);
    this.table.offset = 0;
    this.filterList.emit({
      filteredList: this.lists
    });
  }

  onButtonClicked() {
    this.reportClicked.emit();
  }

}
