import {Component, OnInit} from '@angular/core';
import {NavbarService} from '../../Services/navbar.service';
import {DataService} from '../../Services/data.service';
import {NavigationEnd, Router} from '@angular/router';
import {AuthService} from '../../Services/auth.service';

@Component({
  selector: 'app-navbar-professeur',
  templateUrl: './navbar-professeur.component.html',
  styleUrls: ['./navbar-professeur.component.css']
})
export class NavbarProfesseurComponent implements OnInit {
  professeur: Professeur;

  constructor(private auth: AuthService,
              public nav: NavbarService,
              private dataService: DataService,
              private router: Router) {

  }

  ngOnInit() {
    this.nav.show();
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.dataService.getCurrentProfile().subscribe(
          professeur => {
            this.professeur = professeur;
          }
        );
      }
    });


  }

  onNavbarClicked() {
    this.nav.toggleNavbar();
  }

  onSettingsClicked() {
    this.nav.isNavbarCollapsed = true;
  }

  onLogoutClicked() {
    this.auth.logout();
    this.nav.hideAll();
  }
}
