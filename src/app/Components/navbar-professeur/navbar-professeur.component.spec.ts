import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NavbarProfesseurComponent} from './navbar-professeur.component';

describe('NavbarProfesseurComponent', () => {
  let component: NavbarProfesseurComponent;
  let fixture: ComponentFixture<NavbarProfesseurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NavbarProfesseurComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarProfesseurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
