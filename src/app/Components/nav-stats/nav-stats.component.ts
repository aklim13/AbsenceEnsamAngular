import {Component, Input, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {NavbarService} from '../../Services/navbar.service';

@Component({
  selector: 'app-nav-stats',
  templateUrl: './nav-stats.component.html',
  styleUrls: ['./nav-stats.component.css']
})
export class NavStatsComponent implements OnInit {
  @Input() title: string;
  @Input() iconType: string;
  @Input() showStats = {'hori': false, 'verti': true, 'donut': false};

  constructor(private location: Location,
              public nav: NavbarService) {
  }

  ngOnInit() {
    this.nav.hide();
  }

  onLocationBack() {
    this.location.back();
  }

  onNavbarClicked() {
    this.nav.toggleNavbar();
  }


  onHorizontalClicked() {
    this.nav.isNavbarCollapsed = true;
    this.showStats.hori = true;
    this.showStats.verti = this.showStats.donut = false;
  }

  onVerticalClicked() {
    this.nav.isNavbarCollapsed = true;
    this.showStats.verti = true;
    this.showStats.hori = this.showStats.donut = false;
  }

  onDonutClicked() {
    this.nav.isNavbarCollapsed = true;
    this.showStats.donut = true;
    this.showStats.hori = this.showStats.verti = false;
  }
}
