import {Component, OnInit} from '@angular/core';
import {DataService} from '../../Services/data.service';
import {NavbarService} from '../../Services/navbar.service';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-profile-prof',
  templateUrl: './profile-prof.component.html',
  styleUrls: ['./profile-prof.component.css']
})
export class ProfileProfComponent implements OnInit {
  professeur: Professeur;
  loadingIndicator = true;
  columns = [];

  constructor(private nav: NavbarService,
              public dataService: DataService) {

  }

  ngOnInit() {
    this.nav.show();
    this.dataService.getCurrentProfile().subscribe(professeur => {
      this.professeur = professeur;
    });
  }

}
