import {Component, OnInit} from '@angular/core';
import {NavbarService} from '../../Services/navbar.service';
import {Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  constructor(public nav: NavbarService,
              public router: Router,
              public cookieService: CookieService) {
  }

  ngOnInit() {
    this.nav.show();
    this.router.navigate([`${this.nav.currentProfile}`]);
  }
}
