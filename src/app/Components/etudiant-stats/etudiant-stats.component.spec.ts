import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EtudiantStatsComponent} from './etudiant-stats.component';

describe('EtudiantStatsComponent', () => {
  let component: EtudiantStatsComponent;
  let fixture: ComponentFixture<EtudiantStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EtudiantStatsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtudiantStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
