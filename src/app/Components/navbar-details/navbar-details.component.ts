import {Component, Input, OnInit} from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'app-navbar-details',
  templateUrl: './navbar-details.component.html',
  styleUrls: ['./navbar-details.component.css']
})
export class NavbarDetailsComponent implements OnInit {
  @Input() title: string;
  @Input() iconType: string;

  constructor(private location: Location) {
  }

  ngOnInit() {
  }

  onLocationBack() {
    this.location.back();
  }


}
