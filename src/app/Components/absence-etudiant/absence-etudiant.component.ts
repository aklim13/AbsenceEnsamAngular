import {Component, OnInit, ViewChild} from '@angular/core';
import {NavbarService} from '../../Services/navbar.service';
import {DataService} from '../../Services/data.service';
import {ExcelService} from '../../Services/excel.service';

@Component({
  selector: 'app-absence-etudiant',
  templateUrl: './absence-etudiant.component.html',
  styleUrls: ['./absence-etudiant.component.css']
})
export class AbsenceEtudiantComponent implements OnInit {
  columns = [];
  loadingIndicator = true;
  absences: Absence[];
  tmpAbsences: Absence[];
  etudiantID: number;
  @ViewChild('myTable') table: any;

  constructor(private nav: NavbarService,
              private excelService: ExcelService,
              private dataService: DataService) {
    this.columns = [
      {prop: 'NomProfesseur', name: 'Professeur'},
      {prop: 'Seance.DetailModule.Module.description', name: 'Module'},
      {prop: 'Seance.DetailModule.Cours.Description', name: 'Cours'},
      {prop: 'Seance.Date', name: 'Date'},
      {prop: 'Seance.NbrSeance', width: 50, name: 'Nombre seance'}
    ];
  }

  ngOnInit() {
    this.nav.show();
    this.dataService.getCurrentProfile().flatMap(
      etudiant => {
        this.etudiantID = etudiant.EtudiantID;
        return this.dataService.getEtudiantAbsencesPourEtudiant(this.etudiantID);
      }
    ).subscribe(absences => {
      this.absences = absences;
      this.tmpAbsences = absences;
    }, err => {
      this.loadingIndicator = false;
    }, () => {
      this.loadingIndicator = false;
    });
  }

  onAbsenceFiltred(data) {
    this.absences = data.filteredList;
  }

  onReportClicked() {

  }
}
