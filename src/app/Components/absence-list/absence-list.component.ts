import {Component, OnInit} from '@angular/core';
import {NavbarService} from '../../Services/navbar.service';
import {ShareDataService} from '../../Services/share-data.service';
import {Location} from '@angular/common';
import {DataService} from '../../Services/data.service';

@Component({
  selector: 'app-absence-list',
  templateUrl: './absence-list.component.html',
  styleUrls: ['./absence-list.component.css']
})
export class AbsenceListComponent implements OnInit {
  profName: string;
  seance: any;
  etudiants: Etudiant[];
  checkedEtudiants: Etudiant[];
  title: string;
  type: string;
  message: string;

  constructor(private nav: NavbarService,
              private dataService: DataService,
              private sharedData: ShareDataService,
              private location: Location) {
  }

  ngOnInit() {
    this.nav.hide();
    this.type = 'info';
    this.title = 'Anuller pointage absence';
    this.message = 'Veuillez selectionner les étudiants qui sont absents';
    this.sharedData.seance.subscribe(
      seance => {
        this.seance = seance;
      }
    );
    this.sharedData.prof.subscribe(
      prof => {
        this.profName = prof;
      }
    );
    this.dataService.getEtudiantByCycle(this.seance.DetailCycleID).subscribe(
      etudiants => {
        this.etudiants = etudiants;
        for (const e of etudiants) {
          e.isToggled = false;
        }
      },
      err => {
        console.log(err);
      },
      () => {
      }
    );
  }

  onSubmit() {
    const absences = [];
    for (const etudiant of this.checkedEtudiants) {
      absences.push({
        SeanceID: this.seance.seanceID,
        EtudiantID: etudiant.EtudiantID,
        NomProfesseur: this.profName
      });
    }
    this.dataService.postAbsence(absences).subscribe(
      res => {
        this.type = 'success';
        this.message = 'Absence enregistré ... vous serez redérigé vers la page d\'acceuil ';
        setTimeout(() => {
          this.location.back();
        }, 3000);
      },
      err => {

      },
      () => {

      }
    );
  }

  onEtudiantListChanged(event) {
    this.checkedEtudiants = this.etudiants.filter(e => e.isToggled);
  }
}
