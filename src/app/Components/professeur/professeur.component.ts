import {Component, OnInit, ViewChild} from '@angular/core';
import {NavbarService} from '../../Services/navbar.service';
import {DataService} from '../../Services/data.service';
import {Router} from '@angular/router';
import {ExcelService} from '../../Services/excel.service';

@Component({
  selector: 'app-professeur',
  templateUrl: './professeur.component.html',
  styleUrls: ['./professeur.component.css']
})
export class ProfesseurComponent implements OnInit {
  @ViewChild('myTable') table: any;
  loadingIndicator = true;
  professeurs: Professeur[];
  tmpProfs: Professeur[];


  constructor(private nav: NavbarService,
              public dataService: DataService,
              private excelService: ExcelService,
              private router: Router) {

  }

  ngOnInit() {
    this.nav.show();
    this.dataService.getProfesseurs().subscribe(
      professeurs => {
        this.tmpProfs = [...professeurs];
        this.professeurs = professeurs;
      },
      (err) => {
        console.log(err);
        this.router.navigate(['/notallowed']);
      },
      () => {
        this.loadingIndicator = false;
      }
    );
  }

  onProfFiltred(data) {
    this.professeurs = data.filteredList;
  }

  onRowActivated(event) {
    if (event.type === 'click') {
      this.router.navigate([`/professeurs/${event.row.ProfesseurID}/modules`]);
    }
  }

  onReportClicked() {
    const profReport = [];
    this.professeurs.forEach(
      prof => {
        profReport.push({
          Nom: prof.AspNetUsers.Nom,
          Prenom: prof.AspNetUsers.Prenom,
          Cin: prof.AspNetUsers.Cin,
          Tel: prof.AspNetUsers.PhoneNumber,
          Email: prof.AspNetUsers.Email,
          Departement: prof.Departement.Description,
          Vacataire: prof.Vacataire ? 'vacataire' : 'permanant'
        });
      }
    );
    this.excelService.exportAsExcelFile(profReport, 'professeurs');
  }
}
