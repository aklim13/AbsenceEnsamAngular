interface Stat {
  name: string;
  value: number;
}

interface Stats {
  Stats: Stat[];
  BeginDate: BeginDate;
  EndDate: EndDate;
}
