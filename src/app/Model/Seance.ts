interface Seance {
  SeanceType: SeanceType;
  DetailModule: DetailModule;
  SeanceID: number;
  SeanceTypeID: number;
  DetailModuleID: number;
  NbrSeance: number;
  Date: Date;
}
