interface DetailCycle {
  Annee: Annee;
  Cycle: Cycle;
  OptionFilliere: OptionFilliere;
  Section: Section;
  Groupe: Groupe;
  DetailCycleID: number;
  AnneeID: number;
  CycleID: number;
  OptionFilliereID: number;
  SectionID: number;
  GroupID: number;
}
