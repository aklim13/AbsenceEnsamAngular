interface DetailEtudiant {
  Cycle: string;
  Annee: string;
  Section: string;
  Filliere: string;
  OptionFilliere: string;
  Groupe: string;
}
