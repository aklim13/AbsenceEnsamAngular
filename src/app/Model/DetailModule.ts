interface DetailModule {
  Module: Module;
  Cours: Cours;
  DetailCycle: DetailCycle;
  Professeur: Professeur;
  DetailModuleID: number;
  ProfesseurID: number;
  DetailCycleID: number;
  CoursID: number;
  ModuleID: number;

}
