interface Etudiant {
  EtudiantID: number;
  Adresse: string;
  Cne: string;
  AspNetUsers: AspNetUsers;
  Parent: Parent;
  DetailCycle: DetailCycle;
  detailEtudiant: DetailEtudiant;
  DetailCycleID: number;
  AbsenceCount: number;
  isToggled: boolean;
}
