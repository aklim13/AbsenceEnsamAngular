interface Parent {
  ParentID: number;
  Cin: string;
  Email: string;
  PhoneNumber: string;
  Adresse: string;
  Nom: string;
  Prenom: string;
}
