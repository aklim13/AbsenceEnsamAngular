interface CurrentProfesseur {
  AspNetUsers: AspNetUsers;
  Departement: Departement;
  ProfesseurID: number;
  DepartementID: number;
  Vacataire: boolean;
}
