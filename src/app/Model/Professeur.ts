interface Professeur {
  AspNetUsers: AspNetUsers;
  Departement: Departement;
  ProfesseurID: number;
  DepartementID: number;
  Vacataire: boolean;
  Empreinte1?: any;
  Empreinte2?: any;
}
