interface BeginDate {
  Day: number;
  Month: number;
  Year: number;
}

interface EndDate {
  Day: number;
  Month: number;
  Year: number;
}
