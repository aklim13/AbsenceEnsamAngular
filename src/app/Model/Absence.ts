interface Absence {
  NomProfesseur: string;
  Seance: Seance;
  AbsenceID: number;
  SeanceID: number;
  EtudiantID: number;
  Etudiant: Etudiant;
}
