import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {CookieService} from 'ngx-cookie-service';
import {AuthService} from '../Services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private cookie: CookieService, private authService: AuthService) {
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.cookie.check('access_token')) {
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }
}
