import {inject, TestBed} from '@angular/core/testing';

import {TofirstpageGuard} from './tofirstpage.guard';

describe('TofirstpageGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TofirstpageGuard]
    });
  });

  it('should ...', inject([TofirstpageGuard], (guard: TofirstpageGuard) => {
    expect(guard).toBeTruthy();
  }));
});
