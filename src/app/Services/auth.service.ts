import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Headers, Http, RequestOptions, URLSearchParams} from '@angular/http';
import {CookieService} from 'ngx-cookie-service';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {

  constructor(private router: Router, private http: Http, private cookieService: CookieService) {
  }

  getTokens(loginData) {
    const headers = new Headers({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8'
    });
    const options = new RequestOptions({headers: headers});
    const params = new URLSearchParams();
    params.set('username', loginData.email);
    params.set('password', loginData.password);
    params.set('grant_type', 'password');
    return this.http.post('http://localhost:54489/Token', params.toString(), options)
      .map(res => res.json());
  }

  sendToFirstPage() {
    switch (this.cookieService.get('role')) {
      case '"Prof"':
        this.router.navigate(['/liste/cours']);
        break;
      case '"Admin"':
        this.router.navigate(['/etudiants']);
        break;
      case '"Etudiant"':
        this.router.navigate(['/etudiant/statistiques']);
        break;
    }
  }


  saveToken(data) {
    const expireDate = data.expires_in;
    this.cookieService.set('access_token', data.access_token, expireDate);
    this.cookieService.set('refresh_token', data.refresh_token, expireDate);
    this.cookieService.set('role', data.userRole, expireDate);
    this.sendToFirstPage();
  }

  isLoggedIn() {
    return this.cookieService.get('access_token').length > 0;
  }

  logout() {
    this.cookieService.deleteAll();
    this.cookieService.deleteAll();
    this.cookieService.deleteAll();
  }

}
