import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import {CookieService} from 'ngx-cookie-service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/concatMap';

@Injectable()
export class DataService {
  public url = 'http://localhost:54489/';
  public cycle: string;

  constructor(public http: Http, private cookie: CookieService) {

  }


  getHeaders() {
    const headers = new Headers({
      'Content-type': 'application/json',
      'Authorization': 'Bearer ' + this.cookie.get('access_token')
    });
    return new RequestOptions({headers: headers});
  }

  getEtudiants() {
    return this.http.get(this.url + 'odata/Etudiants' + '?$expand=AspNetUsers,DetailCycle', this.getHeaders())
      .map(res => res.json().value).publishReplay(20).refCount();
  }

  getReportEtudiants() {
    return this.http.get(this.url + 'odata/Etudiants?$expand=DetailCycle/Annee,DetailCycle/Cycle,DetailCycle/Groupe' +
      ',DetailCycle/OptionFilliere/Filliere,DetailCycle/Section,AspNetUsers', this.getHeaders())
      .map(res => res.json().value);
  }


  getEtudiantsById(id: number) {
    return this.http.get(this.url + `odata/Etudiants(${id})?$expand=AspNetUsers`, this.getHeaders())
      .map(res => res.json());
  }

  getCurrentProfile() {
    return this.http.get(this.url + 'CurrentProfile', this.getHeaders()).map(res => res.json());
  }

  getEtudiantParents(id: number) {
    return this.http.get(this.url + `odata/Etudiants(${id})/Parents`, this.getHeaders())
      .map(res => res.json().value);
  }

  getEtudiantDetailCycle(etudiant: Etudiant) {
    return this.http.get(this.url +
      `odata/Etudiants(${etudiant.DetailCycleID})/DetailCycle?$expand=Annee,Cycle,Section,OptionFilliere/Filliere`,
      this.getHeaders()).map(res => res.json());
  }

  getEtudiantsData(etudiant: Etudiant) {
    const urls = [
      this.getEtudiantCycles(etudiant),
      this.getEtudiantAnnees(etudiant),
      this.getEtudiantFillieres(etudiant),
      this.getEtudiantGroupes(etudiant)
    ];

    if (etudiant.DetailCycle.DetailCycleID <= 4) {
      urls.push(this.getEtudiantSections(etudiant));
    }
    return Observable.forkJoin(urls);
  }

  getEtudiantAbsences(id: number) {
    return this.http.get(this.url +
      `odata/Etudiants(${id})/Absence?$expand=Seance/DetailModule/Module,Seance/DetailModule/Cours,
      Seance/DetailModule/Professeur/AspNetUsers`,
      this.getHeaders()).map(res => res.json().value);
  }

  getEtudiantByCycle(detailCycleID: number) {
    return this.http.get(this.url +
      `odata/Etudiants?$expand=AspNetUsers&$filter=DetailCycleID eq ${detailCycleID}`,
      this.getHeaders()).map(res => res.json().value);
  }

  getEtudiantAbsencesPourEtudiant(id: number) {
    return this.http.get(this.url +
      `odata/Etudiants(${id})/Absence?$expand=Seance/DetailModule/Module,Seance/DetailModule/Cours`,
      this.getHeaders()).map(res => res.json().value);
  }

  getEtudiantAbsencesReport(id: number) {
    return this.http.get(this.url + `odata/Etudiants(${id})/Absence?$expand=Seance/DetailModule/Professeur/Departement,Seance/SeanceType,Seance/DetailModule/Professeur/AspNetUsers,Seance/DetailModule/DetailCycle/Cycle,Seance/DetailModule/DetailCycle/Annee,Seance/DetailModule/DetailCycle/Groupe,Seance/DetailModule/DetailCycle/OptionFilliere,Seance/DetailModule/DetailCycle/Section,Seance/DetailModule/DetailCycle/OptionFilliere/Filliere`,
      this.getHeaders()).map(res => res.json().value);
  }

  getEtudiantCycles(etudiant: Etudiant) {
    return this.http.get(this.url + `/odata/Cycles(${etudiant.DetailCycle.CycleID})`, this.getHeaders())
      .map(res => res.json().Description);
  }

  getEtudiantAnnees(etudiant: Etudiant) {
    return this.http.get(this.url + `/odata/Annees(${etudiant.DetailCycle.AnneeID})`, this.getHeaders())
      .map(res => res.json().description);
  }

  getEtudiantFillieres(etudiant: Etudiant) {
    return this.http.get(this.url + `/odata/OptionFillieres(${etudiant.DetailCycle.OptionFilliereID})?$expand=Filliere`
      , this.getHeaders())
      .map(res => res.json());
  }

  getEtudiantGroupes(etudiant: Etudiant) {
    return this.http.get(this.url + `/odata/Groupes(${etudiant.DetailCycle.GroupID})`, this.getHeaders())
      .map(res => res.json().description);
  }

  getEtudiantSections(etudiant: Etudiant) {
    return this.http.get(this.url + `/odata/DetailCycles(${etudiant.DetailCycleID})/Section`, this.getHeaders())
      .map(res => res.json().Description);
  }

  getEtudiantStatsByWeek(id: number) {
    return this.http.get(this.url + `/StatsEtudiantByWeek?id=${id}`, this.getHeaders())
      .map(res => res.json());
  }

  getEtudiantStatsByDate(id: number, begin: string, end: string) {
    return this.http.get(this.url + `/StatsEtudiantByDate?id=${id}&start=${begin}&end=${end}`, this.getHeaders())
      .map(res => res.json());
  }

  getClasseStatsByWeek(id: number) {
    return this.http.get(this.url + `/StatsEtudiantByWeek?id=${id}`, this.getHeaders())
      .map(res => res.json());
  }

  getClasseStatsByDate(id: number, begin: string, end: string) {
    return this.http.get(this.url + `/StatsEtudiantByDate?id=${id}&begin=${begin}&end=${end}`, this.getHeaders())
      .map(res => res.json());
  }

  getSeances() {
    return this.http.get(this.url +
      'odata/Seances?$expand=DetailModule/Professeur/AspNetUsers,' +
      'DetailModule/DetailCycle,DetailModule/Cours,DetailModule/Module,SeanceType',
      this.getHeaders())
      .map(res => res.json().value);
  }

  getSeanceReport() {
    return this.http.get(this.url + 'odata/Seances?$expand=SeanceType,DetailModule/Professeur/AspNetUsers,' +
      'DetailModule/Professeur/Departement,DetailModule/DetailCycle/Annee,DetailModule/DetailCycle/Groupe,' +
      'DetailModule/DetailCycle/OptionFilliere,DetailModule/DetailCycle/' +
      'Cycle,DetailModule/DetailCycle/Section,DetailModule/DetailCycle/OptionFilliere/Filliere',
      this.getHeaders())
      .map(res => res.json().value);
  }

  getSeance(id: number) {
    return this.http.get(this.url + `/odata/Seances(${id})?$expand=DetailModule/Module,DetailModule/Cours`,
      this.getHeaders())
      .map(res => res.json());
  }

  getSeanceAbsences(id: number) {
    return this.http.get(this.url +
      `odata/Seances(${id})/Absence?$expand=Seance/SeanceType,Etudiant/AspNetUsers`,
      this.getHeaders())
      .map(res => res.json().value);
  }

  getProfesseurs() {
    return this.http.get(this.url + 'odata/Professeurs?$expand=Departement,AspNetUsers', this.getHeaders())
      .map(res => res.json().value);
  }

  getProfesseur(id: number) {
    return this.http.get(this.url + `odata/Professeurs(${id})?$expand=Departement,AspNetUsers`, this.getHeaders())
      .map(res => res.json());
  }

  getProfesseurModules(id: number) {
    return this.http.get(this.url + `odata/Professeurs(${id})/DetailModule?$expand=Cours,Module`, this.getHeaders())
      .map(res => res.json().value)
      .flatMap((modules: DetailModule[]) => {
        if (modules.length > 0) {
          return Observable.forkJoin(
            modules.map((module: any) => {
              return this.http.get(this.url +
                `odata/DetailCycles(${module.DetailCycleID})?$expand=Annee,Cycle,OptionFilliere/Filliere,Section,Groupe`)
                .map((res: any) => {
                  module.DetailCycle = res.json();
                  return module;
                });
            })
          );
        }
        return Observable.of([]);
      });
  }

  getDetailCycles() {
    return this.http.get(this.url + 'odata/DetailCycles?$expand=Annee,Cycle,OptionFilliere/Filliere,Section',
      this.getHeaders()).map(res => res.json().value);
  }

  changePassword(changePassword) {
    return this.http.post(this.url + 'api/Account/ChangePassword', changePassword, this.getHeaders())
      .map(res => res.toString());
  }

  postSeance(seance) {
    return this.http.post(this.url + 'odata/Seances', seance, this.getHeaders())
      .map(res => res.json());
  }

  postAbsence(absences) {
    return this.http.post(this.url + 'noterabsence', absences, this.getHeaders())
      .map(res => res.toString());
  }

}
