import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class ShareDataService {
  private seanceSource = new BehaviorSubject<any>({});
  seance = this.seanceSource.asObservable();
  private profSource = new BehaviorSubject<any>({});
  prof = this.profSource.asObservable();

  constructor() {
  }

  changeSeanceMessage(seance, prof) {
    this.seanceSource.next(seance);
    this.profSource.next(prof);
  }
}
