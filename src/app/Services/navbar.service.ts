import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';

@Injectable()
export class NavbarService {
  isAdmin: boolean;
  isProf: boolean;
  isEtudiant: boolean;

  currentProfile: string;

  isNavbarCollapsed = true;

  constructor(private cookie: CookieService) {
    this.isAdmin = false;
    this.isProf = false;
    this.isEtudiant = false;
  }

  hide() {
    this.isAdmin = false;
    this.isProf = false;
    this.isEtudiant = false;
  }

  hideAll() {
    this.isAdmin = false;
    this.isProf = false;
    this.isEtudiant = false;
  }

  show() {
    switch (this.cookie.get('role')) {
      case '"Prof"':
        this.showProfesseur();
        this.currentProfile = '/parametres/profile/professeur';
        break;
      case '"Admin"':
        this.showAdmin();
        this.currentProfile = '/parametres/profile/admin';
        break;
      case '"Etudiant"':
        this.showEtudiant();
        this.currentProfile = '/parametres/profile/etudiant';
        break;
    }
  }

  toggleNavbar() {
    this.isNavbarCollapsed = !this.isNavbarCollapsed;
  }

  showProfesseur() {
    this.isProf = true;
    this.isEtudiant = this.isAdmin = false;
  }

  showEtudiant() {
    this.isEtudiant = true;
    this.isProf = this.isAdmin = false;
  }

  showAdmin() {
    this.isAdmin = true;
    this.isProf = this.isEtudiant = false;
  }
}
